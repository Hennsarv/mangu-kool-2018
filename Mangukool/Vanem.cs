﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangukool
{
    class Vanem
    {
        static string Filename = @"..\..\andmed\vanemad.txt";
        static Dictionary<string, Vanem> _Vanemad = LoeVanemad();
        public static List<Vanem> Vanemad => _Vanemad.Values.ToList();
        
        string _IK;
        string _Nimi;

        List<string> _Lapsed = new List<string>();
        List<string> Lapsed => _Lapsed.ToList();

        static Dictionary<string, Vanem> LoeVanemad()
        {
            Dictionary<string, Vanem> vanemad = new Dictionary<string, Vanem>();
            string[] read = Filename.Loefile();
            foreach (var rida in read)
            {
                string[] osad = rida.Split(',');
                string ik = osad[0].Trim();
                string nimi = osad.Length > 1 ? osad[1].Trim() : "";
                if (ik != "")
                    if (!vanemad.Keys.Contains(ik))
                        vanemad.Add(ik, new Vanem { _IK = ik, Nimi = nimi});
                for(int i = 2; i < osad.Length; i++)
                {
                    Õpilane.ByIk(osad[i].Trim())?.LisaVanem(ik);
                    vanemad[ik]._Lapsed.Add(osad[i].Trim());
                }
            }
            return vanemad;
        }


        public string IK => _IK;
        public string Nimi
        {
            get => _Nimi;
            private set => _Nimi = value.NormNimi();
        }

        public static Vanem ByIk(string ik)
        {
            return _Vanemad.Keys.Contains(ik) ? _Vanemad[ik] : null;
        }

        public override string ToString()
        {
            string vastus =  $"lapsevanem {Nimi}";
            if (_Lapsed.Count > 0)
            {
                vastus += " (lapsed:";
                foreach (var x in _Lapsed) vastus += (" " + Õpilane.ByIk(x)?.Nimi);
                vastus += ")";
            }
            return vastus;
        }


    }
}
