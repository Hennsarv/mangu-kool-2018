﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mangukool
{
    // see on üks (võimalik) näide, kuidas üks klass võiks välja näha
    class Õppeaine
    {
        // staatilised väljad - täidetakse enne teisi
        // failinimi, kust andmed lugeda ja dictionary, kuhu lugeda
        static string Filename = @"..\..\andmed\õppeained.txt";
        static Dictionary<string, Õppeaine> Õppeained = LoeÕppeained();
        //NB! vaata allpool - see on minu täna väljamõeldud kavalus
        // staatilise dictionary loen alguses automeetselt failist 
        // (või hiljem andmebaasist)
        // VB! see EI OLE kõige parem viis, kuna tekitab segadust

        public static List<Õppeaine> Ained { get => Õppeained.Values.ToList(); }
        // dictionary ise on kinni (private), aga õppeainete list on lahtine (static property)

        // õppeaine koosneb kahest väljast, mis väljapoole on readonly
        string _Kood;      // fieldid
        string _Nimetus;

        public string Kood { get => _Kood; }   // ja neile vastavad getiga properid
        public string Nimetus { get => _Nimetus; }

        // hää on kui igal klassil on oma ToString()
        public override string ToString()
        {
            return $"({_Kood}) {_Nimetus}";
        }

        // see nüüd on kavalus, mis ma täna siinsamas välja mõtlesin
        // ise ka pole varem nii teinud
        // staatiline meetod, mis loeb failist õppeained sisse
        // ja mis kutsutakse välja staatilise dictionary algväärtsutamisel
        static Dictionary<string, Õppeaine> LoeÕppeained()
        {
            string[] ained = Filename.Loefile();
            Dictionary<string, Õppeaine> dained = new Dictionary<string, Õppeaine>();
            foreach(var rida in ained)
            {
                string kood = rida.Split(',')[0].Trim();
                string nimetus = rida+",".Split(',')[1].Trim();
                if (kood.Trim() != "")
                if (!dained.Keys.Contains(kood)) dained.Add(kood, new Õppeaine { _Kood = kood, _Nimetus = nimetus });
            }
            return dained;
        }

        public static string AineNimi(string kood)
        {
            return Õppeained.Keys.Contains(kood) ? Õppeained[kood]._Nimetus : "";
        }
    }
}
