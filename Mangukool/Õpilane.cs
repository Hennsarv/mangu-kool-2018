﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mangukool
{
    class Õpilane
    {
        static string Filename = @"..\..\andmed\õpilased.txt";
        static Dictionary<string, Õpilane> _Õpilased = LoeÕpilased();
        // algse nulli asemele lisan siia siis selle funktsiooni
        public static List<Õpilane> Õpilased => _Õpilased.Values.ToList();

        string _IK; public string IK => _IK;
        string _Nimi;
        string _Klass; public string Klass => _Klass;
        public string Nimi
        {
            get => _Nimi;
            set { _Nimi = value.NormNimi() ; }
        }

        List<string> _Vanemad = new List<string>();
        List<string> Vanemad => _Vanemad.ToList();

        // muutsin natuke seda meetodit - tegin ta funktsiooniks
        // mis annab vastuseks disctionary, mille ta kokku paneb

        public static Dictionary<string, Õpilane> LoeÕpilased()
        {
            // üleval olnud staatilise dictionary asemel kasutan kohalikku muutujat
            var õpilased = new Dictionary<string, Õpilane>();

            string[] loetudread = File.ReadAllLines(Filename);
            foreach(var rida in loetudread)
            {
                string[] osad = rida.Split(',');
                string ik = osad[0].Trim();
                string nimi = osad[1].Trim();
                string klass = osad[2].Trim();
                if(! õpilased.Keys.Contains(ik) )
                õpilased.Add(ik, new Õpilane { _IK = ik, _Klass = klass, Nimi = nimi });
            }
            return õpilased;  // selle returni lisasin, et oleks funktsioon
        }

        public static Õpilane ByIk(string ik)
        {
            return _Õpilased.Keys.Contains(ik) ? _Õpilased[ik] : null;
        }

        internal void LisaVanem(string ik)
        {
            if (!_Vanemad.Contains(ik)) _Vanemad.Add(ik);
        }

        public override string ToString()
        {
            string vastus =  $"õpilane {Nimi}";
            if (_Vanemad.Count > 0)
            {
                vastus += "(vanemad:";
                foreach (var x in _Vanemad) vastus += (" " + Vanem.ByIk(x)?.Nimi);

                vastus += ")";
            }
            return vastus;
        }
    }
}
