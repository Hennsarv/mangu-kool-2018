﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangukool
{
    class Õpetaja
    {
        static string Filename = @"..\..\andmed\õpetajad.txt";
        static Dictionary<string, Õpetaja> _Õpetajad = LoeÕpetajad();
        public static List<Õpetaja> Õpetajad => _Õpetajad.Values.ToList(); 
      

        string _IK;
        string _Nimi;
        string _Ainekood;         // mis aine õpetaja on
        string _Klassikood = "";  // kus on klassijuhataja

        static Dictionary<string, Õpetaja> LoeÕpetajad()
        {
            Dictionary<string, Õpetaja> õpetajad = new Dictionary<string, Õpetaja>();
            string[] read = Filename.Loefile();
            foreach (var rida in read)
            {
                string[] osad = rida.Split(',');
                string ik = osad[0];
                string nimi = osad.Length > 1 ? osad[1].Trim() : "";
                string ainekood = osad.Length > 2 ? osad[2].Trim() : "";
                if (ik != "")
                    if (!õpetajad.Keys.Contains(ik))
                        õpetajad.Add(ik, new Õpetaja { _IK = ik, Nimi = nimi, _Ainekood = ainekood });
            }
            return õpetajad;
        }


        public string IK => _IK; 
        public string Nimi {
            get => _Nimi;
            private set => _Nimi = value.NormNimi() ; 
        }

        public string Klassikood => _Klassikood; 

        public string ÕppeaineNimi
        {
            get => Õppeaine.AineNimi(_Ainekood);
        }

        public override string ToString()
        {
            return $"{ÕppeaineNimi} õpetaja {Nimi} {(_Klassikood == "" ? "" : $"({_Klassikood} klassi juhataja)")}";
        }


    }
}
