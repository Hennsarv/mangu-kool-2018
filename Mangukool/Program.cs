﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangukool
{
    class Näidiseks
    {
        // private field eksponeeritud läbi publik getteri
        // tegemist on readonly propertyga
        private string _Sisu;
        public string Sisu
        {
            get
            {
                return _Sisu;
            }
        }
        // sama asi, aga lühemalt kirja pandud
        private string _Nisu; public string Nisu => _Nisu;

        // public field ja public property
        public string Misu; // see on väli - ilmutatud getterit ja setterit
                            // tal ei ole
        public string Pisu { get; set; }  // see on property
                                          // ilmutatud kujul private fieldi tal ei ole

        private string _Kisu; public string Kisu { get => _Kisu; private set => _Kisu = value; }
        // public readonly property, millel on private setter

        public string Lisu { get; private set; }
        // sama, mis eelmine ilma ilmutatud private fieldita

        public readonly string Jusu;
        // see on nö readonly field - võib nii public kui private olla
        // sellele saab väärtust anda AINULT konstruktorist

        public const string Musu = "musu"; // väli, mida muuta ei saa

    }


    class Program
    {
        static void Main(string[] args)
        {
            foreach (var x in Õppeaine.Ained)
                Console.WriteLine(x);

            foreach (var x in Õpetaja.Õpetajad) Console.WriteLine(x);

            Õpilane kiir = new Õpilane { Nimi = "jorch adniel" };
            Console.WriteLine(kiir);

            //Õpilane.LoeÕpilased(); // see loeb õpilased majja - enam pole vaja - klass ise loeb oma 
            // asjad failist

            // vanemaid ei ole enne kui me neid ei vaata!
            Console.WriteLine(Vanem.Vanemad.Count);

            foreach (var x in Õpilane.Õpilased) Console.WriteLine(x);

            foreach (var x in Vanem.Vanemad) Console.WriteLine(x);

            //Console.WriteLine(Õpilane.ByIk("50503070215").Klass);
            Õpilane õ = Õpilane.ByIk("50503070215");

            Console.WriteLine($"{õ.Nimi} õpib {õ.Klass} klassis");

            #region MyRegion
            /*
                if (false) // selle tegin silumiseks, et vaatan kas vanemate failist lugemine õnnestub
                {
                    string[] testVanemad = @"..\..\andmed\vanemad.txt".Loefile();
                    foreach (var rida in testVanemad)
                    {
                        //Console.WriteLine(rida);
                        string[] osad = rida.Split(',');
                        Console.WriteLine($"{osad[1].Trim()} (ik: {osad[0].Trim()}) on sellised lapsed");
                        for (int i = 2; i < osad.Length; i++)
                            Console.WriteLine(Õpilane.ByIk(osad[i].Trim())?.Nimi ?? osad[i]);
                    }
                }
    */

            #endregion        



        }

    }
}
