﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mangukool
{
    // hiljem seletan
    static class Utilities
    {
        public static string NormNimi(this string nimi)
        {
            string[] nimed = nimi.Split(' ');
            if (nimed.Length <2)
            return nimi == "" ? "nimeeitea" : nimi.Substring(0, 1).ToUpper() + nimi.Substring(1).ToLower();
            else
            {
                for (int i = 0; i < nimed.Length; i++)
                {
                    nimed[i] = nimed[i].NormNimi();
                }
                return string.Join(" ", nimed);
            }
        }

        public static string[] Loefile(this string filename) => File.ReadAllLines(filename);
        

        
    }
}
