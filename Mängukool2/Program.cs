﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mängukool2  // namespace - et klasside nimed oleks mingis süsteemis
{
    class Program // selle klassi nimi on Mängukool2.Program
    {
        // programm (consoli rakendusest) peab koosnema vähemalt ühest klassist
        // ja selle sees on staattiline meetod Main - käivitatakse Main
        static void Main(string[] args)  // see siin on meetod
        {
            //lausete jada - nelja liiki

            string nimi; // see lause defineerib muutuja, mida saab allpool kasutada
                         // meenutame mõistet LOKAALNE muutuja
                         // lokaalse muutuja nimi väikeste tähtedega - SOOVITUS!

            Console.WriteLine("Tere!"); // meetodi väljakutse
                                        // meetod TEEB midagi
                                        // meetod paikneb mingis klassis
                                        // meetodile antakse parametrite väärtused ette ("Tere")
            nimi = Console.ReadLine();  // avaldis "=" on tehtemärk
                                        // funktsiooni väljakutse (ReadLine)
                                        // funktsioon on nagu meetid, aga tema käest saab vastuse (retrun avaldis)
                                        // meetod ei anna vastust (void)

            // lausete blokid

            // lihtsaim blokk
            {
                // see on lihtsalt "sülle võetud" ports lauseid
                // selle blokis kirjeldatud muutujad on näha ainult selle bloki sees
            }

            //keerukamad blokid koosnevad PÄISEST ja KEHAST

            if (nimi == "Henn")   // if bloki päis
            { // siit
                // siin laused, mis täidetakse vaid siis kui nimi on "Henn"
            } // siiani on if bloki keha
            // if lausele võib järgneda else blokk või lause
            else
            { // see siin on else blokk
                // kui see koosneb ainult 1st lausest, võib loogelised sulud ära jätta

            }

            // enamasti on bloki PÄIS alguses ja siis on keha
            // on üks lause, kus blokil on saba

            do
            {
                nimi = Console.ReadLine();
            } while (nimi == "Sarv");  // do blokil on olemas SABA

            // me tunneme praegu veel selliseid blokke
            /*
             * while
             * for
             * foreach
             * else if on else blokk, mis sisaldab if lauset
             * switch
             * 
             * HOIATAN - blokke on veel
             * using, try catch, checked-unchecked, locked, ...
             * 
             * blokkide moodi on ka 
             * class, void Meetodid, funktsioonid, jpm...
             * { }
             * 
             * KÕIK LAUSED on kas blokid või lõpevad ;
             * 
             */
        }
    }


}
