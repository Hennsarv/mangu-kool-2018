﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mängukool3
{
    class MisOnKlass
    {
        // klassi sisu e komponendid e klassi liimed
        // andmeväljad (field)
        // meetodid või funktsioonid

        public string Nimi; // klassi sisene muutuja
                            // aga erinevalt lokaalsest 
                            // (meetodi/funktsiooni sisesest)
                            // "paistab" klassi muutuja "kaugemale"
                            // kirjutame reeglina suure tähega (püstine kaamel)
        public int Vanus;   // selle on olemas VAIKIMISI väärtus

        private string _Salajane = "salasõna";

        public void Vanane(int mitu)
        {
            Vanus += mitu;
        }

        public void Noorene(int mitu)
        {
            Vanus = Vanus < mitu ? 0 : Vanus - mitu;
        }
        public override string ToString()
        {
            // kolm varianti
            return
                // 1. lihtne avaldis
                //Nimi + " on nii vana " + Vanus.ToString();

                // 2. keeruline avaldis
                //string.Format("{0} on nii vana {1}", Nimi, Vanus);

                // 3. interpoleeritud string - algab dollariga
                $"{Nimi} on nii vana {Vanus}";

        }

        // juurdepääsu määrtlus - käib nii fieldide (muutujate)
        // kui ka meetodite ja funktsioonide kohta

        // private - private asju näevad ainult teised meetodid ja avaldised 
        //      selles klassis

        // public - public asju näevad kõik
        //      selle klassi asjad
        //      selle programmi asjad
        //      muud asjad (väljs pool meie programmi)

        // internal - sisemised, neid näevad meie programmi asjad
        //      selles klassis
        //      teised klassid samas programmis

        // protected - ei oska tõlkida, sest kaitstud ei täehdna sama asja
        //      Inimene -> Õpilane
        //      Inimene -> Õpetaja
        //      protected asju näevad tuletatud klassides olevad asjad
        // sellest me ei pea veel praegu aru saama

        // kui meil on miski väli, mis on private

        private string Salajane;

        // private välja exponeerimine
        public string getSalajane() // getter
        {
            return Salajane;
        }

        public void setSalajane(string x)  // setter
        {
           // if (kontroll)
                Salajane = x;
        }

        // C# on getterite ja setterite asemele property

        public string Avalik
        {
            get { return Salajane; }
            internal set { Salajane = value; }
        }
    }
}
