﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mängukool3
{
    class Program
    {
        static void Main(string[] args)
        {
            // meil on aegajalt ülesanne - me ei leiuta ise asju
            // ülesanne oli ...
            // appi - mis see klass oli
            MisOnKlass muutuja = new MisOnKlass();
            // instantsi loomiseks (väärtuse eksemplari loomiseks)
            // on new korraldus
            int arv = 70;
            string nimi = "Henn";

            muutuja.Vanus += 10;

            muutuja.Nimi = "muutuja";
            muutuja.Vanane(7);

            Console.WriteLine(muutuja);


        }
    }
}
